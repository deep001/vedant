 <?php
require_once __DIR__ . '/../../autoload/define.php';
use App\Classes\Config;

?>
<!-- META TAGS -->
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="image/logo-2.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
   	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet"> 
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="/../<?php echo Config::path()->ASSETSFRONT ;?>/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="/../<?php echo Config::path()->ASSETSFRONT ;?>/css/materialize.css" rel="stylesheet">
	<link href="/../<?php echo Config::path()->ASSETSFRONT ;?>/css/style.css" rel="stylesheet">
	<link href="/../<?php echo Config::path()->ASSETSFRONT ;?>/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="/../<?php echo Config::path()->ASSETSFRONT ;?>/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->


