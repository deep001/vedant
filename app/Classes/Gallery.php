<?php 

namespace App\Classes;
use App\Traits\Databasetraits;


class Gallery
{
	

    use Databasetraits;
  
    public function getGallery()
	  {
		   $this->db->query("select * from Gallery order by galleryid ASC");
           $exe =  $this->db->execute();
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	  }
	public function deleteGalleryImage($galleryid)
	{
		$this->db->query("DELETE FROM `Gallery` WHERE galleryid=:GID");
		$exe = $this->db->execute(array(
		':GID' => $galleryid,
		));
		if($exe)
		{
			return(object)[
				  'status'=>true,
				  'msg'=>"successfully delete gallery image"
				];
		}
		else {
			return(object)[
				   'status'=>false,
				   'msg'=>"failed ! try again"
				];
		}
			
	}
		public function getGalleryContent($galleryid)
	{
		$this->db->query("select * from Gallery WHERE galleryid=:GID");
		$exe = $this->db->execute(array(
		':GID' => $galleryid,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	}
	public function updateGalleryDetail($galleryid,$GalleryTitle,$smalldesc,$fulldesc,$author,$updatedatesubmission,$file_name1, $file_tmp1)
	{
		if (isset($galleryid) && isset($GalleryTitle)  && isset($file_name1)  && isset($smalldesc)  && isset($fulldesc)  && isset($author)  && isset($updatedatesubmission)) {
               $GalleryTitle = trim(strtolower(filter_var($GalleryTitle, FILTER_SANITIZE_STRING)));
			$smalldesc = trim(strtolower(filter_var($smalldesc, FILTER_SANITIZE_STRING)));
			$fulldesc = trim(strtolower(filter_var($fulldesc, FILTER_SANITIZE_STRING)));
			$author = trim(strtolower(filter_var($author, FILTER_SANITIZE_STRING)));
              //  $firstcontent = trim(filter_var($firstcontent, FILTER_SANITIZE_STRING));
			   $file_name1 = trim(filter_var($file_name1, FILTER_SANITIZE_STRING));
		 if (!Validation::validateGalleryTitle($GalleryTitle)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Gallery Title"
                ];
              
            }
			else if (!Validation::validateSmallDescription($smalldesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Small Description"
                ];
              
            }
			else if (!Validation::validateFullDescription($fulldesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Full Description"
                ];
              
            }
			else if (!Validation::validateAuthor($author)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Author Name"
                ];
              
            }
		
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `Gallery` set `galleryname1`=:TITLE,`galleryimage1`=:IMG,`smalldesc`=:SMALL,`fulldesc`=:FULL,`Date`=:DD,`Author`=:AUTH WHERE `galleryid`=:GID");
				$update = $this->db->execute(array(
                    ':TITLE' => $GalleryTitle,
					 ':IMG' => $file_name1,
					':GID' => $galleryid,
					':SMALL' => $smalldesc,
					':FULL' => $fulldesc,
					':DD' => $updatedatesubmission,
					':AUTH' => $author
                ));
			
				 if($update)
			  {
					 move_uploaded_file( $file_tmp1,"/../assets/images/blog/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'userreturnid' => $galleryid,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content or image ! please try again"
                ];
                }
	} 
	
    public function addGalleryDetail($GalleryTitle,$smalldesc,$fulldesc,$author,$updatedatesubmission,$file_name1, $file_tmp1)
	{
			if (isset($GalleryTitle)  && isset($file_name1)) {
				 $GalleryTitle = trim(strtolower(filter_var($GalleryTitle, FILTER_SANITIZE_STRING)));
				$smalldesc = trim(strtolower(filter_var($smalldesc, FILTER_SANITIZE_STRING)));
			$fulldesc = trim(strtolower(filter_var($fulldesc, FILTER_SANITIZE_STRING)));
			$author = trim(strtolower(filter_var($author, FILTER_SANITIZE_STRING)));
                $file_name1 = trim(filter_var($file_name1, FILTER_SANITIZE_STRING)); 
				 if (!Validation::validateGalleryTitle($GalleryTitle)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Gallery Title"
                ];
              
            }
				else if (!Validation::validateSmallDescription($smalldesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Small Description"
                ];
              
            }
			else if (!Validation::validateFullDescription($fulldesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Full Description"
                ];
              
            }
			else if (!Validation::validateAuthor($author)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Author Name"
                ];
              
            }
				else
				{
					$this->db->query("INSERT INTO `Gallery`(`galleryname1`,`galleryimage1`,`smalldesc`,`fulldesc`,`Date`,`Author`) VALUES (:TITLE,:IMG,:SMALL,:FULL,:DD,:AUTH)");
					
				$insert = $this->db->execute(array(
                    ':TITLE' => $GalleryTitle,
					 ':IMG' => $file_name1,
				     ':SMALL' => $smalldesc,
					':FULL' => $fulldesc,
					':DD' => $updatedatesubmission,
					':AUTH' => $author
                ));
			
				 if($insert)
			  {
					 move_uploaded_file( $file_tmp1,"/../assets/images/blog/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Added to Gallery",	
                    
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Addition ! Please try again"
                ];
                }
				}
				
					
			}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content or image ! please try again",
                ];
                }
	}
}