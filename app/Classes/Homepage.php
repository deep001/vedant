<?php 

namespace App\Classes;
use App\Traits\Databasetraits;


class Homepage
{
	

    use Databasetraits;
  
     public function getContentRecordFirst()
	  { 
     $this->db->query("select * from homepagecontentfirst order by id DESC");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();
     return $row;
  }
	}
	
	public function getHomePageContentFirst($id)
	{
		$this->db->query("select * from homepagecontentfirst WHERE id=:ID");
		$exe = $this->db->execute(array(
		':ID' => $id,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	}
	
	public function updateHomePageRecord($id,$title, $firstcontent, $AboutFirstcontent, $AboutSecondcontent, $file_name1, $file_tmp1)
	{
		if (isset($id) && isset($title) && isset($firstcontent) && isset($file_name1)) {
               $title = trim(strtolower(filter_var($title, FILTER_SANITIZE_STRING)));
                $firstcontent = trim(filter_var($firstcontent, FILTER_SANITIZE_STRING));
			   $file_name1 = trim(filter_var($file_name1, FILTER_SANITIZE_STRING));
		 if (!Validation::validateTitle($title)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Title"
                ];
              
            }
		 else if (!Validation::validateFirstContent($firstcontent)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Content"
                ];
              
            }
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `homepagecontentfirst` set `title`=:TITLE,`image`=:IMG,`Content`=:FIRSTCONTENT, `Fullcontent`=:ABOUTFIRST, `FinalFullContent`=:ABOUTFULL WHERE `id`=:ID");
				$update = $this->db->execute(array(
                    ':TITLE' => $title,
					 ':IMG' => $file_name1,
					 ':FIRSTCONTENT' => $firstcontent,
					 ':ABOUTFIRST' => $AboutFirstcontent,
					 ':ABOUTFULL' => $AboutSecondcontent,
					 ':ID' => $id,
                ));
			
				 if($update)
			  {
					 move_uploaded_file( $file_tmp1,"/../assets/images/about/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'userreturnid' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content or image ! please try again"
                ];
                }
	}
     
  

}