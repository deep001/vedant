<?php 

namespace App\Classes;
use App\Traits\Databasetraits;

session_start();

class EventProcess
{
   use Databasetraits;
    public function getEventRecord()
	  { 
     $this->db->query("select * from eventlist where useremail=:USERMAIL and userid=:ID order by eventid DESC");
     $exe =  $this->db->execute(array(
        ":USERMAIL" => $_SESSION['u_email'],
		":ID"       => $_SESSION['uid']
         
    ));
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetchAll();
     return $row;
      }
	}
	
	public function getAllEventRecord()
	{
	 $this->db->query("select * from eventlist order by eventid DESC");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetchAll();
     return $row;
      }
	}
	public function getCompleteEventRecord($eventid)
	{
		$this->db->query("select * from eventlist where eventid=:EVENID");
     $exe =  $this->db->execute(array(
        ":EVENID" => $eventid,
	));
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();
     return $row;
      }
	}
}