
<?php 
require_once __DIR__ . '/autoload/define.php';
use App\Classes\Config;
use App\Classes\Login;
use App\Classes\Headers; 
?>
<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/jquery.min.js"></script>
<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/angular.min.js"></script>
	<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/bootstrap.js" type="text/javascript"></script>
	<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/materialize.min.js" type="text/javascript"></script>
	<script src="<?php echo Config::path()->ASSETSFRONT ;?>/js/custom.js"></script>
    <link href="<?php echo Config::path()->ASSETSFRONT ;?>/css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="<?php echo Config::path()->ASSETSFRONT ;?>/js/jquery.alerts.js"></script>
<?php
  $role=trim($_POST['role']);
  $name = trim($_POST['name']);
  $email = trim($_POST['email']);
  $password = trim($_POST['password']);
  $userrole = trim(filter_var($role, FILTER_SANITIZE_STRING));
  $username = trim(filter_var($name, FILTER_SANITIZE_STRING));
  $email = trim(filter_var($email, FILTER_SANITIZE_EMAIL));
  $userpassword = trim(filter_var($password, FILTER_SANITIZE_STRING));
  
  $adduser = new Login();
  $adduserdetail = $adduser->addUserDetail($userrole,$username,$email,$userpassword);
  if($adduserdetail->status == true)
			{
				
              
	
			   $success =  $adduserdetail->msg; 
			//   $_SESSION['u_email'] = $Email; 
			//   Headers::redirect("/practicerubico/home.php");
	 // jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert'); 
/*<script>alert('Please fill previous data');</script>*/
			   	
            }
			
			else if ($adduserdetail->status == false) {
                $responseerror =  $adduserdetail->msg;
                  
            }
  
 
  

  ?>

<div id="tabs-2">
                       <?php
						echo (isset($success))? '<h4><div class="alert alert-primary" style="color:green;padding:95px;">'.$success.', <a href="/login">click here for login..</a></div></h4>':'';
						echo (isset($responseerror))? '<h4><div class="alert alert-primary" style="color:red;padding:95px;">'.$responseerror.'</div></h4>':'';
	                    ?>
</div>